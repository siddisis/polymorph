#!/usr/bin/env python

'''
Convert output from Bed file after intersec with SnpEff information to tab file format.

'''

import sys

def get_args(cline = sys.argv[1:]):
    '''
    '''
    import argparse
    parser = argparse.ArgumentParser(description = '''Convert output from VCF.snEff to
                                     tab file format.''')
    parser.add_argument('input_file', help = 'VCF.snpEff output file from SnpSift')
    parser.add_argument('-o', '--output', help = 'output tab file [default: stdout]',
                        metavar = "fname")
    args = parser.parse_args(cline)
    return args
	
	
def convert_line(line):
    '''
	Convert line of bed file with snpEff information to tab format.
	ex:
	Input (SnpEff information part): NS=120;AF=0.575;GC=24,54,42;EFF=SYNONYMOUS_CODING(LOW|SILENT|cgA/cgC|R20|246|YWHAB||CODING|NM_003404.4|3|1) 
	Output: 120 0.575 24,54,42 EFF=SYNONYMOUS_CODING LOW SILENT cgA/cgC R20 246 YWHAB   CODING NM_003404.4 3 1 
	'''
     
    new_line = ""
    line_info = line.split('\t')
    info = line_info[7].split(";")[:-1]
    snpeff = line_info[7].split(";")[-1]

    info_list = []
    for inf in info:
        info_list.append(inf.split("=")[-1])

    snpeff_list = [snpeff.split("(")[0].split("=")[-1]]
    snpeff_list = snpeff_list + snpeff.split("(")[-1].split("|")[:-1]
    snpeff_list.append(snpeff.split("(")[-1].split("|")[-1].split(')')[0])

    newline = str.join('\t', [ str.join('\t', line_info[:5]), str.join('\t', info_list), str.join('\t', snpeff_list),\
                               str.join('\t', line_info[8:])])
    
    return newline
    
def convert_file(input_file, tab_file = sys.stdout):
    '''Converts bed file after intersect with VCF.SnpEff to tab format.

    Input:
      input_file: file handle to read INPUT file
      tab_file: file handle to write tab output (default: sys.stdout)
    '''
    
    
    while True:
        line = input_file.readline()
        if not line:
            break        
            
        tab_file.write(convert_line(line) + '\n')
		
#####################################################################################################
		
if __name__ == '__main__':
    args = get_args()
    file = open(args.input_file, 'r')
    if args.output:
        tab_file = open(args.tab_file, 'w')
        convert_file(file, tab_file)
    else:
        convert_file(file)
