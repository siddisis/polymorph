#!/bin/bash


<<instruction 

Run link.py python program on all file in a folder
It's need 2 argument : Input folder with to process and output folder for output file  

instruction

VARIANT_FILE=$1
PROTEIN_DIR=$2

for filename in `ls $PROTEIN_DIR/*`
do
    echo "/mnt/lustre/home/y.fourne/polymorph/linkage_phospho/link.py  $VARIANT_FILE -r $filename" | \
    qsub -l h_vmem=4g -N `basename $filename` -o $PROTEIN_DIR -j y -cwd -V
done
