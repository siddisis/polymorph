#!/usr/bin/env python

'''
Get fasta file information without sequence

'''

import sys, os

def get_args(cline = sys.argv[1:]):
    '''
    '''
    import argparse
    parser = argparse.ArgumentParser(description = '''Get fasta file information without sequence.''')
    parser.add_argument('input_file', help = 'fasta file from Pfam and SMART website')
    parser.add_argument('-s', '--source', help = 'website source of fasta file download')
    parser.add_argument('-o', '--output', help = 'output VCF file [default: stdout]',
                        metavar = "fname.txt")
    args = parser.parse_args(cline)
    return args


def splicing_smart (line):

	info = line.split()
	code = info[0].split('|')
	pos = code[3].split('/')
	newline = str.join('\t', [code[2], pos[0], pos[1], (str.join(' ', info[1:]))])

	return newline

def splicing_pfam(line):
	
	info = line.split()
	code = info[0].split('/')
	newline = str.join('\t', [code[0][1:], code[1], (str.join(' ', info[1:]))])

	return newline

def get_info(input_file, src, tab=sys.stdout):
	
	file = open(input_file, 'r') 
	if src == 'smart':

		while True:
			line = file.readline()
			if not line:
				break
			if line[0] == '>':
				tab.write(splicing_smart(line) + '\n')
	elif src == 'pfam':
		while True:
			line = file.readline()
			if not line:
				break
			if line[0] == '>':
				tab.write(splicing_pfam(line) + '\n')

#####################################################################################################

if __name__ == '__main__':
    args = get_args()
    if args.output:
        tab = open(args.tab, 'w')
        get_info(args.input_file, args.source, tab)
    else:
        get_info(args.input_file, args.source)






