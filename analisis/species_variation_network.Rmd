---
title: "Species Variation network"
author: "Yannick Fourne"
date: "August 28, 2014"
output: html_document
---

Influence of variation between species in human phsophorylation domain 

### Pfam domain

```{r, echo=FALSE}
dir_path <- "/Users/Sedjro/Desktop/internship/Species_file/phospho_domain/useful_snp"
file_list <- dir(dir_path, pattern = "PF" )

dom_dist <- NULL
freq_list <- NULL

for (file in file_list){
  dom_name <- strsplit(file, "-PF")[[1]][1]
  data <- read.delim(paste(dir_path, file, sep = '/'), header = FALSE)
  snp_num <- nrow(data) 
  dom_dist <- rbind(dom_dist, data.frame(dom_name, snp_num)) 
  freq_list[[dom_name]] <- data$V7
}
list_dom <- names(freq_list)

```

```{r SNP-per-pfam_Domain, echo=FALSE, results='asis'}
row.names(dom_dist) <- NULL
barplot(dom_dist$snp_num, names.arg = dom_dist$dom_name, 
        main = "Pfam species variation distibution", col = rgb(0,0,1,1/4), las =2)
```



### SMART domain

```{r, echo=FALSE}
dir_path <- "/Users/Sedjro/Desktop/internship/Species_file/phospho_domain/useful_snp"
file_list <- dir(dir_path, pattern = "smart" )

dom_dist <- NULL
freq_list <- NULL

for (file in file_list){
  dom_name <- strsplit(file, "-smart")[[1]][1]
  data <- read.delim(paste(dir_path, file, sep = '/'), header = FALSE)
  snp_num <- nrow(data) 
  dom_dist <- rbind(dom_dist, data.frame(dom_name, snp_num)) 
  freq_list[[dom_name]] <- data$V7
}
list_dom <- names(freq_list)

```

```{r SNP-per-SMART_Domain, echo=FALSE, results='asis'}
row.names(dom_dist) <- NULL
barplot(dom_dist$snp_num, names.arg = dom_dist$dom_name, 
        main = "SMART species varaition distibution", col = rgb(1,0,0, 1/4), las = 2)
```




```{r info}
sessionInfo()
```
