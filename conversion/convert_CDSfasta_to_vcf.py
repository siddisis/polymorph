#!/usr/bin/env python

'''
Convert output from CDS fasta to VCF file format.
VCF file format description: http://samtools.github.io/hts-specs/VCFv4.2.pdf
'''

import sys, re
from Bio import SeqIO
from Bio.Seq import Seq

def get_args(cline = sys.argv[1:]):
   '''
   '''
   import argparse
   parser = argparse.ArgumentParser(description = '''Convert output from CDS fasta to
                                    VCF file format.''')
   parser.add_argument('input_file', help = 'VCF output file from CDS fasta')
   parser.add_argument('-r', '--reference', help = 'alignment reference species assembly')
   parser.add_argument('-c', '--comparison', nargs = '+', help = 'List of alignment comparison species assembly')
   parser.add_argument('-o', '--output', help = 'output VCF file [default: stdout]',
                       metavar = "fname.vcf")
   args = parser.parse_args(cline)
   return args


def add_header(fname, species_ref, species_comp):
   '''
   Writes the proper VCF header to fname.
   
   Input
   fname: file handle
   '''
    
   header = '''\
##fileformat=VCFv4.2
##source=CDS fasta
##description=Alignment VCF file
##reference={0}
##comparison={1}
##INFO=<ID=GN,Number=1,Type=String,Description="geneName-The name field from the genePred table">
##INFO=<ID=EN,Number=1,Type=Integer,Description="Exon Number-Exons are counted starting at one and begin at the transcription start site">
##INFO=<ID=TE,Number=1,Type=Integer,Description="Total Exons-The number of coding exons in the gene">
##INFO=<ID=EL,Number=1,Type=Integer,Description="Exon Length-The length of the current exons">
##INFO=<ID=IF,Number=1,Type=Integer,Description="InFrame-The frame number of the first nucleotide in the exon">
##INFO=<ID=OF,Number=1,Type=Integer,Description="outFrame-The frame number of the nucleotide after the last nucleotide in this exon">
##INFO=<ID=VP,Number=1,Type=Integer,Description="Variant position on Exon">
##FORMAT=<ID=GT,Number=1,Type=String,Description="Species Genotype">
#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t{1}\n'''.format(species_ref, str.join('\t', species_comp))

    
   fname.write(header)


def convert_line(species_ref, species_comp, comparison):
   '''Convert four lines of CDS fasta to VCF format.

   Input:
   line: a string of text
   chr: the chromosome (string)
    
   Example:
   convert_line('hg19', ['panTro2', 'rheMac2'],
   {'>NM_032291_hg19_2_25 64 1 2 chr1:67091530-67091593+' : 'GATTGAAAAAACGTACAAGGAAGGCCTTTGGAATACGGAAGAAAGAAAAGGACACTGATTCTAC',
   '>NM_032291_panTro2_2_25 64 1 2 chr1:67803187-67803250+' : 'GACTGAAAAAACGTACAAGGAAGGCCTTTGGAATACGGAAGAAAGAAAAGGACACTGATTCTAC',
   '>NM_032291_rheMac2_2_25 64 1 2 chr1:69414133-69414196+' : 'GATTGAAAAAACGTACAAGGAAGGCCTTTGGAATACGGAAGAAAGAAAAGGACACTGATTCTAC'}
   
   returns 'chr1    67091530        chr1:67091530       T       C       PASS    .       GN=NM_032291;EN=2;TE=25;EL=64;IF=1;OF=2;VP=3      CP      1/1     0/0'
   '''
   
   #Get SNPs variation
   new_line = ""
   for k in comparison.keys():
       if re.search(species_ref, k):
          seq_length = len(comparison[k])
          spe_desc_ref = k

   ############################################################       
   #Cheking sequences orientation
   if spe_desc_ref[-1] == '-':
      for cle in comparison.keys():
         comparison[cle] = comparison[cle].reverse_complement()
   ############################################################
         
   dic_ref = {}
   dic_spe = {}
   for spe in species_comp:
      nucl_pos = None
      dic_alt = {}
   
      for ky in comparison.keys():
         if re.search(spe, ky):
            spe_desc_comp = ky

      nucl_ref = ""
      nucl_alt = ""
      for position in range(seq_length):
         if comparison[spe_desc_ref][position].upper() != comparison[spe_desc_comp][position].upper(): 
            nucl_ref += comparison[spe_desc_ref][position].upper()
            nucl_alt += comparison[spe_desc_comp][position].upper()
            if nucl_pos == None:
               nucl_pos = position
        
         if comparison[spe_desc_ref][position].upper() == comparison[spe_desc_comp][position].upper() and nucl_pos != None:
            if 'A' not in nucl_ref and 'T' not in nucl_ref and 'G' not in nucl_ref and 'C' not in nucl_ref:
               nucl_ref = '-' + nucl_alt
            if 'A' not in nucl_alt and 'T' not in nucl_alt and 'G' not in nucl_alt and 'C' not in nucl_alt:
               nucl_alt = '-' + nucl_ref
            if nucl_pos in dic_ref:
               if dic_ref[nucl_pos] < nucl_ref:
                  dic_ref[nucl_pos] = nucl_ref
            else:
               dic_ref[nucl_pos] = nucl_ref
               
            dic_alt[nucl_pos] = nucl_alt    
            nucl_ref = ""
            nucl_alt = ""
            nucl_pos = None

      dic_spe[spe] = dic_alt
   
   #New line creation
   new_line = ""
   for line in sorted(dic_ref):
      for key in comparison.keys():
         if re.search(species_ref, key):
            VP = line 
            REF =  dic_ref[line] 
            split_ref_one = key.split(' ') 
            split_ref_two = split_ref_one[0].split('_')
            chrom = split_ref_one[-1].split(':')[0]
            pos =  split_ref_one[-1].split(':')[-1].split('-')[0]
            Vpos = int(pos) +  VP
            snpID = chrom + ':' + str(Vpos)
            GN = str.join('_',[split_ref_two[0], split_ref_two[1]])
            INFO = str.join(';', ['GN=' + GN, 'EN=' + split_ref_two[-2], 'TE=' + split_ref_two[-1],\
                                  'EL=' + split_ref_one[1], 'IF=' + split_ref_one[2], 'OF=' + split_ref_one[3], 'VP=' + str(VP + 1)])

      genotype = []
      seq_alt = []
      for spe in species_comp:
         if line in dic_spe[spe]:
            if dic_spe[spe][line] not in seq_alt:
               seq_alt.append(dic_spe[spe][line])
            gent = seq_alt.index(dic_spe[spe][line]) + 1
         else:
            gent = 0

         genotype.append((str(gent) + '/' + str(gent)))

      GT = str.join('\t', genotype)      
      ALT = str.join(',', seq_alt)
      new_line = new_line + str.join('\t',[chrom, str(Vpos), snpID, REF, ALT, "PASS", '.', INFO, 'GT', GT]) + '\n'

   return new_line
    
    
def convert_CDSfasta_to_vcf(input_file,  species_ref, species_comp, vcf_file = sys.stdout):
   '''
   Converts CDS fasta file to VCF format.

   Input:
   CDS fasta file: file handle to read fasta file
   vcf_file: file handle to write VCF output (default: sys.stdout)
   '''
    
   add_header(vcf_file, species_ref, species_comp)

   comparison = {}
   for seq_record in SeqIO.parse(input_file, "fasta"):
      
      if re.search(species_ref, seq_record.description):
         comparison[seq_record.description] = seq_record.seq
         
      for spe in species_comp:
         if re.search(spe, seq_record.description):
            comparison[seq_record.description] = seq_record.seq

      if len(comparison) == len(species_comp) + 1:
         vcf_file.write(convert_line(species_ref, species_comp, comparison))
         comparison = {}



def get_gzip(impute_file):
    
    import gzip 
    file_name = (impute_file.split('/')[-1]).split('.')[-1]
    if file_name == 'gz':
        content = gzip.open(impute_file, 'rb') 
    else:
        content = open(impute_file, 'rb')

    return content
    
#####################################################################################################
            
if __name__ == '__main__':
    args = get_args()
    input_file = get_gzip(args.input_file)
    if args.output:
        vcf_file = open(args.vcf_file, 'w')
        convert_CDSfasta_to_vcf(input_file,  args.reference, args.comparison, vcf_file)
    else:
        convert_CDSfasta_to_vcf(input_file,  args.reference, args.comparison)
